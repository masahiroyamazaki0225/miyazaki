$('.HeaderSection-Inner-HamburgerMenuBtn').on('click', function (e) {
    $('.MenuLayer').toggleClass('isActive');
});

var Villa001_SubFrameGroup_Swiper = new Swiper('.Villa001ContentsSection-Inner-MainColumn-SubFrameGroup .swiper-container', {
    loop : true,
    autoplay: true,
    slidesPerView : 3,
    spaceBetween : 20,
    centeredSlides : true,
    loopedSlides: 1,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    on: {
        slideChange: function(){
            Villa001_MainImage_Swiper.slideToLoop(this.realIndex)
        }
    }
});
var Villa001_MainImage_Swiper = new Swiper('.Villa001ContentsSection-Inner-MainColumn-MainImage .swiper-container', {
    loop : true,
    slidesPerView : 1,
    loopedSlides: 1,
    effect: 'fade',
    on: {
        slideChange: function(){
            Villa001_SubFrameGroup_Swiper.slideToLoop(this.realIndex)
        }
    }
});

var Villa002_SubFrameGroup_Swiper = new Swiper('.Villa002ContentsSection-Inner-MainColumn-SubFrameGroup .swiper-container', {
    loop : true,
    autoplay: true,
    slidesPerView : 3,
    spaceBetween : 20,
    centeredSlides : true,
    loopedSlides: 1,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    on: {
        slideChange: function(){
            Villa002_MainImage_Swiper.slideToLoop(this.realIndex)
        }
    }
});
var Villa002_MainImage_Swiper = new Swiper('.Villa002ContentsSection-Inner-MainColumn-MainImage .swiper-container', {
    loop : true,
    slidesPerView : 1,
    loopedSlides: 1,
    effect: 'fade',
    on: {
        slideChange: function(){
            Villa002_SubFrameGroup_Swiper.slideToLoop(this.realIndex)
        }
    }
});
var Villa003_SubFrameGroup_Swiper = new Swiper('.Villa003ContentsSection-Inner-MainColumn-SubFrameGroup .swiper-container', {
    loop : true,
    autoplay: true,
    slidesPerView : 3,
    spaceBetween : 20,
    centeredSlides : true,
    loopedSlides: 1,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    on: {
        slideChange: function(){
            Villa003_MainImage_Swiper.slideToLoop(this.realIndex)
        }
    }
});
var Villa003_MainImage_Swiper = new Swiper('.Villa003ContentsSection-Inner-MainColumn-MainImage .swiper-container', {
    loop : true,
    slidesPerView : 1,
    loopedSlides: 1,
    effect: 'fade',
    on: {
        slideChange: function(){
            Villa003_SubFrameGroup_Swiper.slideToLoop(this.realIndex)
        }
    }
});


var RoomsAboutSectionSwiper = new Swiper('.RoomsAboutSection .swiper-container', {
    loop : true,
    slidesPerView : 1,
    loopedSlides: 1,
    effect: 'fade',
    autoplay: true,
    speed: 1000
});
