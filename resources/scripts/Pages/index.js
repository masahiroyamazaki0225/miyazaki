$('.HeaderSection-Inner-HamburgerMenuBtn').on('click', function (e) {
    $('.MenuLayer').toggleClass('isActive');
});

$(window).on('scroll', function (event) {

    var Y = window.scrollY;
    if(100 < window.scrollY){
        $('.SNSFooterLayer').addClass("isActive");
        $('.SideSNSLayer').addClass("isActive");
    } else{
        $('.SNSFooterLayer').removeClass("isActive");
        $('.SideSNSLayer').removeClass("isActive");
    }

    //AboutSection
    if ($('.AboutSection').offset().top <= Y + innerHeight / 2) {
        $('.AboutSection-Frame-Inner-TextBox').addClass('isShown');
    }
    //StaySection
    if ($('.StaySection').offset().top <= Y + innerHeight / 2) {
        $('.StaySection-Frame-Inner-TextBox').addClass('isShown');
    }
    //LandscapeSection
    if ($('.LandscapeSection').offset().top <= Y + innerHeight / 2) {
        $('.LandscapeSection-Frame-Inner-TextBox').addClass('isShown');
    }
    //ActivitySection
    if ($('.ActivitySection').offset().top <= Y + innerHeight / 2) {
        $('.ActivitySection-Frame-Inner-TextBox').addClass('isShown');
    }

});


var BigimageSectionSwiper = new Swiper('.BigImageSection .swiper-container', {
    navigation: {
        nextEl: '.BigImageSection .swiper-button-next',
        prevEl: '.BigImageSection .swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
    },
    slidesPerView: 1,
    spaceBetween: 0,
    spped: 1000,
    loop: true,
    effect: 'fade',
    autoplay: {
        delay: 4500,
    },
});

var SlideContentsSectionSwiper = new Swiper('.SlideContentsSection .swiper-container', {
    navigation: {
        nextEl: '.SlideContentsSection .swiper-button-next',
        prevEl: '.SlideContentsSection .swiper-button-prev',
    },
    slidesPerView: 4,
    spaceBetween: 0,
    loop: true,
    autoplay: {
        delay: 2000,
    },
    breakpoints: {
        768: {
            slidesPerView: 1,
            spaceBetween: 40
        }
    }
});


// (function () {
//     var slides = $('.BigImageSection-Frame-Inner');
//     var indicator = 0;
//     var max_slide_num = slides.length - 1;
//
//     slides.eq(max_slide_num).addClass('isPrev');
//     slides.eq(indicator).addClass('isCurrent');
//
//     setInterval(function () {
//
//         slides.eq(indicator).removeClass('isCurrent');
//
//         if (indicator == 0) {
//             slides.eq(max_slide_num).removeClass('isPrev');
//         } else {
//             slides.eq(indicator - 1).removeClass('isPrev');
//         }
//
//         indicator = (indicator + 1) % slides.length; //次のスライドを指し示す数
//
//         if (indicator == 0) {
//             slides.eq(max_slide_num).addClass('isPrev');
//         } else {
//             slides.eq(indicator - 1).addClass('isPrev');
//         }
//
//         slides.eq(indicator).addClass('isCurrent');
//
//     }, 3000);
// }());






